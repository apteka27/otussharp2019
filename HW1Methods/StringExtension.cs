﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1Methods
{
    public static class StringExtension
    {
        /// <summary>
        /// Расширение для типа  string
        /// возвращает небольшую часть текста песни
        /// </summary>
        /// <param name="text">текст песни</param>
        /// <returns></returns>
        public static string ShortLyrics(this string text)
        {

            return text.Substring(0, 50) + " .........";
        }
    }
}
