﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HW1Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            //логируем все необработанные исключения
            //если в параметрах установлено  Debug = True , то еще выводим ошибку на экран
            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            Console.WriteLine("Загружаем музыкальные треки");
            int i = 0;

            string MusicPath = Directory.GetCurrentDirectory() + "\\Music";
            Console.WriteLine(MusicPath);

            PlayList PL1 = new PlayList();
            PlayList PL2 = new PlayList();
            string[] fileEntries = Directory.GetFiles(MusicPath);
            MusicTrack mt;
            foreach (string fileName in fileEntries)
            {
                i++;
                mt = new MusicTrack(fileName);
                Console.WriteLine(fileName);
                if (i <= 2)
                {

                    PL1.Addtrack(mt);
                }
                else
                {
                    PL2.Addtrack(mt);
                }
            }


            Console.WriteLine("Тесты ");
            Console.WriteLine("------------------------------");

            Player P = new Player();
            Console.WriteLine("Тесты для ToString музыкального трека и индексатора");
            Console.WriteLine(PL1[1].ToString());

            Console.WriteLine("Нажмите любую клавишу");
            Console.ReadKey();


            Console.WriteLine("------------------------------");
            Console.WriteLine("Собрали два плэйлиста");
            Console.WriteLine("Тесты для ToString лэйлистов");
            Console.WriteLine("Плэйлист 1");
            Console.WriteLine(PL1);

            Console.WriteLine("Плэйлист 2");
            Console.WriteLine(PL2);

            Console.WriteLine("------------------------------");
            Console.WriteLine("Проигрываем плэйлист 1");
            P.PlayPlayList(PL1);



            Console.WriteLine("Нажмите любую клавишу");
            Console.ReadKey();
            Console.WriteLine("------------------------------");
            Console.WriteLine("Проигрываем одну песню из плэйлиста 2");

            P.PlayTrack(PL2[2]);
            Console.WriteLine("Нажмите любую клавишу");



            Console.WriteLine("------------------------------");
            Console.WriteLine("Тест расширения для string ");
            Console.WriteLine("Выводим только часть текста песни");
            Console.WriteLine("------------------------------");

            string text = String.Join(Environment.NewLine, PL2[2].Lyrics);
            Console.WriteLine(text.ShortLyrics());

            Console.ReadKey();

        }
        /// <summary>
        /// Ловим ошибки при выполнении программы. 
        /// все ошибки выводм в консоль при отладке и записываем ошибку в лог файл
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {

            var logger = new SimpleLogger();
            logger.Fatal(e.ExceptionObject.ToString());

            if (Properties.Settings.Default.Debug == true)
            {
                Console.WriteLine(e.ExceptionObject.ToString());
                Console.WriteLine("Press Enter to continue");
                Console.ReadLine();
            }

            Environment.Exit(1);
        }



    }
}
