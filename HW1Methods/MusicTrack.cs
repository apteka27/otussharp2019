﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HW1Methods
{
    /// <summary>
    /// Класс для хранения одной музыкальной дорожки 
    /// </summary>
    class MusicTrack
    {
        public string Title;
        public string[] Lyrics;

        /// <summary>
        /// конструктор класса для музыкального трека
        /// </summary>
        /// <param name="file">полный путь к музыкальному треку</param>
        public MusicTrack(string file)
        {
            if (!File.Exists(file))
            {
                throw new Exception("Ошибка чтения файла");
            }
            else
            {
                Lyrics = File.ReadAllLines(file, Encoding.GetEncoding(1251));
                Title = Path.GetFileNameWithoutExtension(file);
            }
        }

        //перегружаем стантартный метод ToString ()
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(Title);
            sb.AppendLine("");

            foreach (string s in Lyrics)
                sb.AppendLine(s);

            return sb.ToString();
        }
    }
}
