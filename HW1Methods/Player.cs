﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1Methods
{
    class Player
    {

        /// <summary>
        /// Воспроизводит один трек
        /// </summary>
        /// <param name="track">Список воспроизведения </param>
        public void PlayTrack(MusicTrack track)
        {
            //пауза между строками песни мс
            int pause = 100;

            Console.WriteLine("----------------------");
            Console.WriteLine(track.Title);
            Console.WriteLine("----------------------");
            foreach (string s in track.Lyrics)
            {
                Console.WriteLine(s);
                System.Threading.Thread.Sleep(pause);
            }
        }

        /// <summary>
        /// воспроизводит плэйлист
        /// </summary>
        /// <param name="playlist"></param>
        public void PlayPlayList(PlayList playlist)
        {

            foreach (MusicTrack track in playlist.Tracks)
            {
                System.Threading.Thread.Sleep(1000);
                Console.WriteLine("----------------------");
                Console.WriteLine("Проигрываю следующий трек");
                Console.WriteLine("----------------------");
                PlayTrack(track);
            }
        }

    }


}
