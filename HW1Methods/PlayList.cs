﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HW1Methods
{
    class PlayList
    {
        public List<MusicTrack> Tracks = new List<MusicTrack>();

        /// <summary>
        /// Индексатор для списка вопроизведения
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public MusicTrack this[int i]
        {

            get
            {
                if (i >= Tracks.Count)
                    throw new IndexOutOfRangeException();

                return Tracks.ElementAt(i);
            }


        }
        /// <summary>
        /// Перегружаем метод ToString ()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (MusicTrack t in Tracks)
                sb.AppendLine(t.Title);
            return sb.ToString();
        }
        /// <summary>
        /// Метод для добаления в список воспроизведения нового трека
        /// </summary>
        /// <param name="m"></param>
        public void Addtrack(MusicTrack m)
        {
            Tracks.Add(m);
        }
        public void ShuffleList()
        {
            throw new Exception("Еще не реализовано");
        }

        public void SaveListToFile()
        {
            throw new Exception("Еще не реализовано");
        }

        public void LoadList()
        {
            throw new Exception("Еще не реализовано");
        }

    }

}
